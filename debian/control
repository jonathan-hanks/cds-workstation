Source: cds-workstation
Section: misc
Priority: optional
Maintainer: Ryan Blair <ryan.blair@ligo.org>
Uploaders:
 Jameson Graef Rollins <jameson.rollins@ligo.org>,
 Jonathan Hanks <jonathan.hanks@ligo.org>,
Build-Depends:
 debhelper (>= 9.0.0),
Standards-Version: 3.9.8
Homepage: https://git.ligo.org/cds-packaging/cds-workstation
Vcs-Git: https://git.ligo.org/cds-packaging/cds-workstation.git
Vcs-Browser: https://git.ligo.org/cds-packaging/cds-workstation

Package: cds-workstation
Architecture: all
Depends:
 cdsutils,
 dataviewer,
 epics-catools,
 gds-crtools,
 git,
 gitk,
 gpstime,
 guardctrl,
 guardian,
 ipython,
 ipython3,
 krb5-user,
 ligo-remote-epics-scripts,
 medm,
 nds2-client,
 ndscope,
 openssh-client,
 pydv,
 python-ezca,
 python-git,
 python-gpstime,
 python-nds2-client,
 python-pyepics,
 python3-ezca,
 python3-git,
 python3-gpstime,
 python3-nds2-client,
 python3-pyepics,
 striptool,
 ${misc:Depends},
Breaks:
 apt (<< 0.7.25.1),
Description: LIGO CDS workstation metapackage
 LIGO CDS workstation metapackage pulls in almost everything needed to
 run a CDS workstation computer (including remote workstations):
 .
  * EPICS (LIGO-patched + remote access) (including CA command line tools)
  * medm (LIGO-patched)
  * pyepics
  * pcaspy
  * striptool
  * NDS2 client
  * ndscope
  * GDS (diaggui, foton, awg, etc.)
  * guardian (including guardctrl)
  * gpstime
  * cdsutils
  * python-ezca
  * pydv
